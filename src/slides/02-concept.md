# Initial Project Analysis Revealed...
- 
    - All the data are available in database tables
    - Manual comparison (how they did it before) is about writing a `SQL JOIN` statement
    - `JOIN` key is at least Taxpayer ID and observed time interval

# Pain Points 
- 
    - Requires both DB developer and Tax expert involvement at the same time
    - Queries run long as they compare the entire year of data for every taxpayer
    - As a result,  <b>Tax Experts</b> were performing the audit on data that was small enough to be exported into MS Excel

---

# Approach Taken 
- 
    - Create a tool so that DB developers can describe (annotate) database (DWH) tables
    - Same tool to provide GUI to <b>Tax Experts</b> for defining comparison which will later generate and run the `SQL` query that will do the job
    - For the sake of performance, comparison should run only on modified data


---

# Derived Restrictions
- For the sake of algorithm traceability following additional restrictions were introduced
    - Split cross-matching process to two phases
        - <b>Planning</b>: where the application will detect modified data for a specified period (also will address the case for running cross-check for specified subset of taxpayers)
        - <b>Execution</b>: where actual cross-matching rules will execute to resolve deviations

    - Generate `Stored Procedures` instead of `SQL` queries
    - Generated `SP`s must be versioned and kept after run 

---
# Defining an Audit Rule (1/2)
- Creating an audit rule implies following steps
    1. Define comparison unit, single document, monthly (or quarterly,annual)
    2. Define subset of taxpayers rule should run on
    3. Define data that is being used for audit
    4. Define expression on when this this check is violated 
     (Example  `ABS([VAT Returns].[Field 2a] * 0.2 - [Received Invoices].[Field 1]) > 100`)
    5. Define expression resulting deviation 
     (Example  `[VAT Returns].[Field 2a] * 0.2 - [Received Invoices].[Field 1]`)
    6. Select the reference fields to be extracted if check passes

---
# Defining an Audit Rule (2/2)
- ...technically
    1. Define comparison unit <code>means defining the key that tables join</code>
    2. Define subset of taxpayers rule should run on <code>means having some condition in WHERE clause</code>
    3. Define data that is being used for audit <code> is about the tables to be joined</code>
    4. Define expression on when this this check is violated <code>is about main clause in WHERE statement</code>
    5. Define expression resulting deviation <code>is about the value to be selected</code>
    6. Select the reference fields to be extracted if check passes <code>is about other values to be selected too</code>

---
# Executing an Audit 
- During planning phase
    - All data tables are checked for new (or modified records) and their respective `KEYs` are saved
    - First step is done only for predefined subset of taxpayers

- During execution
    - For each record saved from planning phase data is checked 
    - If check passes the deviation and references values are appended to record
    - Record is marked as processed

---
# Tables

![Tables](./images/tables.png)

---
# Data Flow - Planning

![Planing](./images/planning.png)

---
# Planning Pseudo-SQL

    INSERT INTO [Result table] ( ...JOIN FIELDS...)
    SELECT ...JOIN FIELDS...
    FROM [Data Table 1] INNER JOIN [Precondition Table] USING (...JOIN FIELDS...)
    WHERE [Data Table 1].][DateTime Field] BETWEEN p_interval_start AND p_interval_end
        AND [precondition filters] 

    UNION 
    ....
    SELECT ...JOIN FIELDS...
    FROM [Data Table N] INNER JOIN [Precondition Table] USING (...JOIN FIELDS...)
    WHERE [Data Table N].][DateTime Field] BETWEEN p_interval_start AND p_interval_end 
        AND [precondition filters] 
---
# Data Flow - Execution

![Running](./images/running.png)

---
# Execution Pseude-SQL

    FOR r IN (SELECT * FROM [Result table] WHERE RULE_ID = x AND STATUS = 'UNPROCESSED')
    LOOP
        EXIT WHEN [condition when execution should be paused]

        BEGIN
            SELECT [deviation expression], [...ref values]
                INTO [local variables]
            FROM 
                [DataTable 1] 
                    INNER JOIN [DataTable 2 ] USING (...JOIN KEY)
                    ... 
                    INNER JOIN [DataTable N ] USING (...JOIN KEY)
            WHERE [rule check expression];

            UPDATE [Result table] 
            SET ...collected data 
            WHERE ID = r.ID;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                UPDATE [Result table]  SET STATUS='NOT FOUND'  WHERE ID = r.ID;
            WHEN OTHERS  THEN
                UPDATE [Result table]  SET STATUS='ERROR', MSG = [error info]  WHERE ID = r.ID;
        END

    END LOOP;

---
# Domain Model (Core Part)

- High level model consists of following entities
    - Rule 
    - Joinable (abstraction for any DB table)
    - Field (column of any database table)
    - FilterOption - represents any condition that can be applied to a field
    - JoinOption - Link for two Fields that are related 

---

## Class: Rule

    public class Rule {

        private Long id;
        private String name;
        private String description;
        private String deviationFormula;
        private String conditional;
        private List<Joinable> joinables; // list of all tables Rule needs (Data and Precondition)
        private List<Field> joinKey; // 
        private List<Field> ruleFields; // Fields this Rule uses
        private Map<FilterOption, List<String>> appliedFilters;


---

## Class: Joinable

    public class Joinable {

        private Long id;
        private String title; // Human readable title
        private Type type; // Data, Precondition or Result
        private String source; // SQL source: Table, View or Select clause
        private String alias;  // 
        private List<Field> fields;

---
## Class: Field

    public class Field {

        private Long id;
        private String name; // name in table
        private String alias; 
        private String description; // human readable name
        private boolean valueField; // can be used in expressions
        private boolean selectionField; // represents datatime of data
        private boolean infoField; // can be used as reference field
        private boolean joinField; // is part of table's join key
        private String aggregateFunction; // which aggregation functin to use when table is joined with one with shorter key
        private List<FilterOption> possibleFilterOptions; // conditions that can be applied on this field 
        private FieldDataType dataType; // data type (used in sql code generation)

        public enum FieldDataType {
            NUMERIC, STRING, DATE, DATETIME
        }

---
## Class: FilterOption

    public class FilterOption {

        private Long id;
        private String title; // human readable title
        private String operator; // SQL operand to be used
        private Composition composition; // NONE - filter can be applied once, ALL, ANY - multiple times combined with AND, OR respectively
        private InputType inputType; // how user should enter the filter value (dropdown or input box)
        private List<PredefinedValue> predefinedValues; // if dropdown, some predefined values
        private String queryPredefinedValues; // if dropdown, some SQL query to retrieve options
        private String uiPosition; // where should it appear in UI

        public enum Composition { ALL, ANY, NONE }
        public enum InputType { SELECT, VALUE }
        @Getter @Setter
        public static class PredefinedValue {
            private Long id;
            private String title, value;
        }

---
## Class: FilterOption

    public class JoinOption {
        private Long id;
        private long fieldA;
        private long fieldB;
        private String joinClause;
    }

---
## Editor UI

![Editor ui](./images/rule_demo.png)

---
## Actual Stored Procedure: Planner

<pre>
create PROCEDURE cm_execution_planner_r_7103231(p_eval_id NUMBER) AS
  /*
        Rule: test_rule_demo
        Created: Mar 16, 2020 1:10:03 PM
        Description: demo_test
   */
ld_eval_start DATE;
ld_eval_end DATE;
BEGIN

  SELECT INTERVAL_START, INTERVAL_END
    INTO ld_eval_start, ld_eval_end
  FROM CM_EVAL_RUN
  WHERE id = p_eval_id;

  INSERT INTO cm_eval_run_context (ID, RUN_ID, RULE_ID, PROCESSED , TIN, period_year)

    SELECT
      cm_global_seq.nextval, p_eval_id, 7103231, 0 , tin, p_year
    FROM (
        SELECT DISTINCT decl_w9_custom_8_81_82_163.TIN  tin, decl_w9_custom_8_81_82_163.TAX_YEAR  p_year
        FROM CAMERAL.CAMERAL_DECL_L6  decl_w9_custom_8_81_82_163
                JOIN PRECONDITION precnd
                    ON 
                            decl_w9_custom_8_81_82_163.TIN = precnd.TIN
        WHERE decl_w9_custom_8_81_82_163.RECEIPT_DATE BETWEEN ld_eval_start AND ld_eval_end
                 AND 
        /* Բացառված ՀՎՀՀ-ներ */
        (
                precnd.TIN <> '00000019' 
                 AND 
                precnd.TIN <> '00000027' 
        )
        AND
        /* Կազմակերպություն / անձ */
        (
                precnd.TP_TYPE LIKE 'L' 
        )
        AND
        /* Կազմակերպաիրավական տեսակներ */
        (
                precnd.ENT_TYPE LIKE 'A%' 
        )


    );

END;
</pre>


---
## Actual Stored Procedure: Executor

<pre>
create PROCEDURE cm_executor_r_7103231(p_eval_id NUMBER) AS
  /*
        Rule: test_rule_demo
        Created: Mar 16, 2020 1:10:03 PM
        Description: demo_test
   */

  -- variables
  lv_err_message VARCHAR2(2000);
  -- subprograms
  FUNCTION paused RETURN BOOLEAN
  AS
    p_r NUMBER;
    BEGIN
      SELECT COUNT(*)
        INTO p_r
      FROM cm_eval_run
      WHERE id = p_eval_id AND status = 'PAUSED';

      RETURN p_r = 1;
    END;
  BEGIN

    FOR rt in (SELECT *
                 FROM CM_EVAL_RUN_CONTEXT
                 WHERE run_id = p_eval_id and rule_id = 7103231 and PROCESSED = 0)
    LOOP
      -- exit if pause requested
      EXIT WHEN paused ();

      BEGIN

        SELECT decl_w9_custom_8_81_82_163.FIELD_8_1 + w9_year_1.TAX_INCOME_TOTAL_A 
            into rt.P_DEVIATION
        FROM CM_EVAL_RUN_CONTEXT ctx
                INNER JOIN  CAMERAL.CAMERAL_DECL_L6  decl_w9_custom_8_81_82_163
                    ON
                        ctx.period_year = decl_w9_custom_8_81_82_163.TAX_YEAR
                    AND
                        ctx.TIN = decl_w9_custom_8_81_82_163.TIN
                INNER JOIN  CAMERAL_DECL_W9  w9_year_1
                    ON
                        ctx.TIN = w9_year_1.TIN
                    AND
                        ctx.period_year = w9_year_1.TAX_YEAR
            WHERE ctx.id = rt.id
                AND decl_w9_custom_8_81_82_163.FIELD_8_1 + w9_year_1.TAX_INCOME_TOTAL_A > 0
        ;

        UPDATE CM_EVAL_RUN_CONTEXT
        SET PROCESSED = 2, P_DEVIATION = rt.P_DEVIATION, PROCESS_DATE = SYSDATE, USER_MODE = 'SYSTEM'
        
        WHERE id = rt.id;
        COMMIT;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
            -- looks ok as conditional didn't met
            UPDATE CM_EVAL_RUN_CONTEXT
            SET PROCESSED = 1, PROCESS_DATE = SYSDATE, USER_MODE = 'SYSTEM'
            WHERE id = rt.id;
        COMMIT;
        WHEN OTHERS  THEN
            lv_err_message := SUBSTR (SQLERRM, 1, 2000);
          UPDATE CM_EVAL_RUN_CONTEXT
          SET PROCESSED = -1, PROCESS_DATE = SYSDATE, ERROR_MSG = lv_err_message, USER_MODE = 'SYSTEM'
          WHERE id = rt.id;
          COMMIT;
      END;


    END LOOP;

    -- at last mark run finished if there no other task
    UPDATE cm_eval_run r
    SET r.STATUS = 'FINISHED'
    WHERE id = p_eval_id and not exists (
      SELECT 1 FROM CM_EVAL_RUN_CONTEXT
      WHERE RUN_ID = p_eval_id
            and PROCESSED = 0
            AND ROWNUM = 1
    );

  END;
</pre>