# Cameral Tax Audit
## Fast expert analytics
### with metaprogramming




       (use ◀️ arrows ▶️ or swipe for navigattion)

---
# Intro
* Cameral Audit is a type of tax audit done by Tax Authorities (with no site visit)
* It is performed by cross-matching Taxpayer's newly submitted tax returns with
    - other submitted documents
    - invoices (whole sale and retail)
    - imported/exported goods data
    - data received from third parties

* Performed periodically, once new data becomes available

---

# The Goal
- Create a tool for Tax Experts that will allow them define and run tasks that will find mistmatches in Taxpayer's data
- Final result of audit should generate list of mistmaches with
    - Taxpayer's information
    - References to mismatching documents 
    - Numeric difference of mistmatch

---

# Restrictions
- 
    - Creating and running comparison rules should require no IT skills 
    - Should be possible to run tasks for any subset of Taxpayers 
    - Run fast





